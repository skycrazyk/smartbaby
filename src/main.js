import vuetify from 'vuetify';
import 'typeface-roboto/index.css';
import 'material-design-icons/iconfont/material-icons.css';
import 'vuetify/dist/vuetify.css';
import 'babel-polyfill';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/';
import cordovaLoader from './cordovaLoader';

Vue.config.productionTip = false;

Vue.use(vuetify);

cordovaLoader(() => {
  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app');
});
