import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Catalog from './views/Catalog.vue';
import Quiz from './views/Quiz.vue';
import Card from './views/Cards.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/cards/:id',
      name: 'card',
      component: Card,
      props: true,
    },
    {
      path: '/cards',
      name: 'catalog',
      component: Catalog,
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/quiz',
      name: 'quiz',
      component: Quiz,
    },
  ],
});
