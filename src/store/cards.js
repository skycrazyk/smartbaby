const init = () => ({
  items: [
    {
      id: 'animals',
      items: [
        'cat',
        'dog',
        'deer',
        'lion',
      ],
    },
    {
      id: 'colors',
      items: [
        'red',
        'orange',
        'yellow',
        'green',
        'cyan',
        'blue',
        'purple',
      ],
    },
  ],
});

export default {
  state: init(),
  namespaced: true,
};
